﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class XInput : MonoBehaviour
{
    // === API ===
    // --- Mouse ---

    public enum MouseButton {
        Left,
        Right,
        Middle,
        None
    }

    public static bool getMouseButtonState(MouseButton inButton) {
        return Input.GetMouseButton(getUnityCodeFrom(inButton));
    }

    public static void collectMouseButtonStates(
        out EnumBitMask<MouseButton> outButtons)
    {
        outButtons = new EnumBitMask<MouseButton>();
        outButtons.set(MouseButton.Left, getMouseButtonState(MouseButton.Left));
        outButtons.set(MouseButton.Right, getMouseButtonState(MouseButton.Right));
        outButtons.set(MouseButton.Middle, getMouseButtonState(MouseButton.Middle));
    }

    // === IMPLEMENTATION ===

    private static MouseButton getMouseButtonFrom(int inUnityCode) {
        switch (inUnityCode) {
            case 0:    return MouseButton.Left;
            case 1:    return MouseButton.Right;
            case 2:    return MouseButton.Middle;
        }
        XUtils.check(false, "Incorrect mouse button code");
        return MouseButton.None;
    }

    private static int getUnityCodeFrom(MouseButton inMouseButton) {
        switch (inMouseButton) {
            case MouseButton.Left:     return 0;
            case MouseButton.Right:    return 1;
            case MouseButton.Middle:   return 2;
        }
        XUtils.check(false, "Incorrect mouse button");
        return int.MinValue;
    }
}
