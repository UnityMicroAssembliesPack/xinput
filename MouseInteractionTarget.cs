﻿using UnityEngine;

//TODO: Force moving of object to appropriate layer for mouse actions tracking
public class MouseInteractionTarget : MonoBehaviour
{
    //API
    //-Events
    public OnMouseEnterEvent onMouseEnter;
    public OnMouseOverEvent onMouseOver;
    public OnMouseExitEvent onMouseExit;

    //Methods
    //-Manager API
    internal void processMouseEnter() {
        onMouseEnter?.Invoke();
    }

    internal void processMouseOver(
        Vector3 inPoint, EnumBitMask<XInput.MouseButton> inMouseButtonStates)
    {
        onMouseOver?.Invoke(inPoint, inMouseButtonStates);
    }

    internal void processMouseExit() {
        onMouseExit?.Invoke();
    }

    //-Debug
#   if UNITY_EDITOR
    private void Awake() {
        bool theHasRenderer = null != XUtils.getComponent<MeshRenderer>(
            this, XUtils.AccessPolicy.JustFind
        );
        if (theHasRenderer) {
            XUtils.getComponent<MouseInteractionTargetDebugHighlighter>(
                this, XUtils.AccessPolicy.CreateIfNo
            );
        }
    }
#   endif
    
    //Types
    public delegate void OnMouseEnterEvent();
    public delegate void OnMouseOverEvent(
        Vector3 inHitPoint, EnumBitMask<XInput.MouseButton> inMouseButtonStates
    );
    public delegate void OnMouseExitEvent();
}
