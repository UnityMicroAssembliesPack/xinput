﻿using UnityEngine;

public class MouseInteractionTargetDebugHighlighter : MonoBehaviour
{
    private void Awake() {
        _meshRenderer = XUtils.getComponent<MeshRenderer>(
            this, XUtils.AccessPolicy.ShouldExist
        );

        var theMouseInteraction = XUtils.getComponent<MouseInteractionTarget>(
            this, XUtils.AccessPolicy.ShouldExist
        );

        theMouseInteraction.onMouseOver +=
            (Vector3 inPoint, EnumBitMask<XInput.MouseButton> inMouseButton) =>
        {
            _meshRenderer.material.color =
                inMouseButton.isAnySet() ? Color.red : Color.yellow;

            //DEBUG CODE {
            //if (!inMouseButton.isAnySet()) return;
            //
            //var theUI = FindObjectOfType<CarCityUIObject>();
            //var theAttachPoint = XUtils.getComponent<WorldObjectAttachPoint>(
            //    this, XUtils.AccessPolicy.ShouldExist
            //);
            //theUI.attachUIForObject(theAttachPoint);
            //}
        };
        theMouseInteraction.onMouseExit += () => {
            _meshRenderer.material.color = idleColor;
        };
    }

    static Color idleColor = Color.white;
    static Color mouseOverColor = Color.yellow;
    static Color mouseClickedColor = Color.red;

    MeshRenderer _meshRenderer = null;
}
