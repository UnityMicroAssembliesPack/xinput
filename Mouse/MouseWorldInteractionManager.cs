﻿using UnityEngine;

public class MouseWorldInteractionManager : MonoBehaviour
{
    void Update() {
        RaycastHit theHitResult;
        bool theHitWasFound = Physics.Raycast(
            Camera.main.ScreenPointToRay(Input.mousePosition), out theHitResult
        );

        MouseInteractionTarget theTarget = null;
        if (theHitWasFound) {
            theTarget = XUtils.getComponent<MouseInteractionTarget>(
                theHitResult.collider, XUtils.AccessPolicy.JustFind
            );
        }

        if (theTarget != _currentTarget) {
            if (_currentTarget) {
                _currentTarget.processMouseExit();
            }
            _currentTarget = theTarget;
            if (_currentTarget) {
                _currentTarget.processMouseEnter();
            }
        }

        if (_currentTarget) {
            EnumBitMask<XInput.MouseButton> theMouseButtonStates;
            XInput.collectMouseButtonStates(out theMouseButtonStates);
            _currentTarget.processMouseOver(
                theHitResult.point, theMouseButtonStates
            );
        }
    }

    private MouseInteractionTarget _currentTarget = null;
}
